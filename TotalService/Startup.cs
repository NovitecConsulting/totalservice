﻿
using Microsoft.Owin;
using Owin;
using TotalService.Models;

[assembly: OwinStartupAttribute(typeof(TotalService.Startup))]
namespace TotalService
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            
            //createRolesandUsers();
        }
        
    }
}
