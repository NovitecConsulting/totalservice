﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReporteVisitasUsuario.aspx.cs" Inherits="TotalService.ReporteVisitasUsuario" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Reporte Visitas Usuario</title>
        <style>
        #contenedor, #Usuario, .boton{
            margin:1em;
        }
       
        .txtUsuario{
            width:350px;
            display:inline;
        }
        #calendarios{
            display:block;
        }
        .calendario{
            display:inline;
        }
        

    </style>

</head>
<body>
    <form id="form1" runat="server">
        <div id="contenedor">
        <div id="Usuario"><asp:Label runat="server">Usuario: </asp:Label>
        <asp:TextBox ID="txtUsuario" runat="server" CssClass="txtUsuario"></asp:TextBox>

        </div>
            <div id="calendarios">
        <asp:Label runat="server">Fecha Inicio: </asp:Label>
        <asp:Calendar ID="Calendar1" runat="server" CssClass="calendario"></asp:Calendar>
         <asp:Button ID="btn1" runat="server" Text="Generate report" onclick="btn1_Click" CssClass="boton"/>
 
        </div>
        </div>

        
        
        
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="891px">
            <LocalReport ReportPath="ReporteVisitas.rdlc">
                
            </LocalReport>
        </rsweb:ReportViewer>
       
    </div>
    </form>
</body>
</html>
