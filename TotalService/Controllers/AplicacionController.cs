﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TotalService.Models;
using WebMatrix.WebData;

namespace TotalService.Controllers
{
    [Authorize]
    public class AplicacionController : Controller
    {
        private grocusmx_totalserviceEntities db = new grocusmx_totalserviceEntities();

        // GET: /Aplicacion/
        public ActionResult Index()
        {
            return View(db.CAT_Aplicacion.ToList());
        }

        // GET: /Aplicacion/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CAT_Aplicacion cAT_Aplicacion = db.CAT_Aplicacion.Find(id);
            if (cAT_Aplicacion == null)
            {
                return HttpNotFound();
            }
            return View(cAT_Aplicacion);
        }

        // GET: /Aplicacion/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Aplicacion/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="IdAplicacion,Nombre,Orden")] CAT_Aplicacion cAT_Aplicacion)
        {
            if (ModelState.IsValid)
            {
                cAT_Aplicacion.FechaCreacion = DateTime.Now;
                cAT_Aplicacion.UsuarioCreacion = WebSecurity.CurrentUserName;

                cAT_Aplicacion.FechaUltimaModificacion = DateTime.Now;
                cAT_Aplicacion.UsuarioUltimaModificacion = WebSecurity.CurrentUserName;

                db.CAT_Aplicacion.Add(cAT_Aplicacion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cAT_Aplicacion);
        }

        // GET: /Aplicacion/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CAT_Aplicacion cAT_Aplicacion = db.CAT_Aplicacion.Find(id);
            if (cAT_Aplicacion == null)
            {
                return HttpNotFound();
            }
            return View(cAT_Aplicacion);
        }

        // POST: /Aplicacion/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="IdAplicacion,Nombre,Orden")] CAT_Aplicacion cAT_Aplicacion)
        {
            CAT_Aplicacion objeto;

            if (ModelState.IsValid)
            {
                objeto = db.CAT_Aplicacion.Find(cAT_Aplicacion.IdAplicacion);

                objeto.FechaUltimaModificacion = DateTime.Now;
                objeto.UsuarioUltimaModificacion = WebSecurity.CurrentUserName;

                db.Entry(objeto).State = EntityState.Modified;
                                
                db.SaveChanges();
                
                return RedirectToAction("Index");
            }
            return View(cAT_Aplicacion);
        }

        // GET: /Aplicacion/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CAT_Aplicacion cAT_Aplicacion = db.CAT_Aplicacion.Find(id);
            if (cAT_Aplicacion == null)
            {
                return HttpNotFound();
            }
            return View(cAT_Aplicacion);
        }

        // POST: /Aplicacion/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CAT_Aplicacion cAT_Aplicacion = db.CAT_Aplicacion.Find(id);
            db.CAT_Aplicacion.Remove(cAT_Aplicacion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
