﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TotalService.Models;

namespace TotalService.Controllers
{

   // [Authorize(Roles = Helpers.Comunes.ROL_Administrador + "," + Helpers.Comunes.ROL_Supervisor)]
    public class VisitadoController : Controller
    {
        private grocusmx_totalserviceEntities db = new grocusmx_totalserviceEntities();

        // GET: /Visitado/
        public ActionResult Index()
        {
            return View(db.OPE_Visitado.ToList());
        }

        // GET: /Visitado/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OPE_Visitado oPE_Visitado = db.OPE_Visitado.Find(id);
            if (oPE_Visitado == null)
            {
                return HttpNotFound();
            }
            return View(oPE_Visitado);
        }

        // GET: /Visitado/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Visitado/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="IdVisitado,NombreEmpresa,NombreResponsable,Direccion,Colonia,Municipio,Telefono,UsuarioCreacion,UsuarioUltimaModificacion,FechaCreacion,FechaUltimaModificacion")] OPE_Visitado oPE_Visitado)
        {
            if (ModelState.IsValid)
            {
                db.OPE_Visitado.Add(oPE_Visitado);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(oPE_Visitado);
        }

        // GET: /Visitado/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OPE_Visitado oPE_Visitado = db.OPE_Visitado.Find(id);
            if (oPE_Visitado == null)
            {
                return HttpNotFound();
            }
            return View(oPE_Visitado);
        }

        // POST: /Visitado/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="IdVisitado,NombreEmpresa,NombreResponsable,Direccion,Colonia,Municipio,Telefono,UsuarioCreacion,UsuarioUltimaModificacion,FechaCreacion,FechaUltimaModificacion")] OPE_Visitado oPE_Visitado)
        {
            if (ModelState.IsValid)
            {
                db.Entry(oPE_Visitado).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(oPE_Visitado);
        }

        // GET: /Visitado/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OPE_Visitado oPE_Visitado = db.OPE_Visitado.Find(id);
            if (oPE_Visitado == null)
            {
                return HttpNotFound();
            }
            return View(oPE_Visitado);
        }

        // POST: /Visitado/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OPE_Visitado oPE_Visitado = db.OPE_Visitado.Find(id);
            db.OPE_Visitado.Remove(oPE_Visitado);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
