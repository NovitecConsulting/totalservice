﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TotalService.Models;
namespace TotalService.Controllers
{
    public class EncuestaController : Controller
    {
        // GET: Encuesta
        public ActionResult Index()
        {
            List<EncuestaModelView> encuestaModelView = new List<EncuestaModelView>();
            using (grocusmx_totalserviceEntities context = new grocusmx_totalserviceEntities())
            {
                encuestaModelView.AddRange(from dbEncuesta in context.CAT_Encuesta
                                           select new EncuestaModelView
                                           {
                                               idEncuesta = dbEncuesta.IdEncuesta,
                                               DescripcionEncuesta = dbEncuesta.DescripcionEncuesta,
                                               UsuarioCreacion = dbEncuesta.UsuarioCreacion,
                                               UsuarioUltimaModificacion = dbEncuesta.UsuarioUltimaModificacion
                                           });
               
            }
            return View(encuestaModelView);
        }


        public ActionResult Details(int id)
        {
            EncuestaModelView encuestaModelView = new EncuestaModelView();
            using (grocusmx_totalserviceEntities context = new grocusmx_totalserviceEntities())
            {
                CAT_Encuesta cAt_Encuesta = context.CAT_Encuesta.Find(id);
                encuestaModelView.DescripcionEncuesta = cAt_Encuesta.DescripcionEncuesta;
                encuestaModelView.UsuarioCreacion = cAt_Encuesta.UsuarioCreacion;
                encuestaModelView.UsuarioUltimaModificacion = cAt_Encuesta.UsuarioUltimaModificacion;
                encuestaModelView.idEncuesta = cAt_Encuesta.IdEncuesta;
                encuestaModelView.listaPreguntas = new List<PreguntaModelView>();
                encuestaModelView.listaPreguntas.AddRange(from dbEncuestaPregunta in context.OPE_EncuestaPregunta
                                                          join dbPregunta in context.CAT_Pregunta 
                                                          on dbEncuestaPregunta.IdPregunta equals dbPregunta.IdPregunta
                                                          where dbEncuestaPregunta.IdEncuesta == encuestaModelView.idEncuesta
                                                          select new PreguntaModelView {
                                                              DescripcionPregunta = dbPregunta.DescripcionPregunta,
                                                              UsuarioCreacion = dbPregunta.UsuarioCreacion,
                                                              idPregunta = dbPregunta.IdPregunta
                                                          });
                foreach (var item in encuestaModelView.listaPreguntas)
                {
                    item.listaRespuestas = new List<RespuestaModelView>();
                    item.listaRespuestas.AddRange(from dbEncuestaPregunta in context.OPE_EncuestaPregunta
                                                  join dbPregunta in context.CAT_Pregunta 
                                                  on dbEncuestaPregunta.IdPregunta equals dbPregunta.IdPregunta
                                                  join dbPreguntaRespuesta in context.CAT_PreguntaRespuesta 
                                                  on dbPregunta.IdPregunta equals dbPreguntaRespuesta.IdPregunta
                                                  join dbRespuesta in context.CAT_Respuesta 
                                                  on dbPreguntaRespuesta.IdRespuesta equals dbRespuesta.IdRespuesta 
                                                  
                                                  where dbEncuestaPregunta.IdPregunta == item.idPregunta
                                                  select new RespuestaModelView
                                                  {
                                                      DescripcionRespuesta = dbRespuesta.DescripcionRespuesta,
                                                      UsuarioCreacion = dbRespuesta.UsuarioCreacion,
                                                      idRespuesta = dbRespuesta.IdRespuesta
                                                  });
                }
            }
            return View(encuestaModelView);
        }
    }
}