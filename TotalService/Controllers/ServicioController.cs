﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TotalService.Models;
using WebMatrix.WebData;

namespace TotalService.Controllers
{

   // [Authorize(Roles = Helpers.Comunes.ROL_Administrador + "," + Helpers.Comunes.ROL_Supervisor)]
    public class ServicioController : Controller
    {
        private grocusmx_totalserviceEntities db = new grocusmx_totalserviceEntities();

        // GET: /Servicio/
        public ActionResult Index()
        {
            return View(db.CAT_Servicio.ToList());
        }

        // GET: /Servicio/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CAT_Servicio cAT_Servicio = db.CAT_Servicio.Find(id);
            if (cAT_Servicio == null)
            {
                return HttpNotFound();
            }
            return View(cAT_Servicio);
        }

        // GET: /Servicio/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Servicio/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CAT_Servicio cAT_Servicio)
        {
            if (ModelState.IsValid)
            {
                cAT_Servicio.FechaCreacion = DateTime.Now;
                cAT_Servicio.UsuarioCreacion = WebSecurity.CurrentUserName;

                cAT_Servicio.FechaUltimaModificacion = DateTime.Now;
                cAT_Servicio.UsuarioUltimaModificacion = WebSecurity.CurrentUserName;


                db.CAT_Servicio.Add(cAT_Servicio);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cAT_Servicio);
        }

        // GET: /Servicio/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CAT_Servicio cAT_Servicio = db.CAT_Servicio.Find(id);
            if (cAT_Servicio == null)
            {
                return HttpNotFound();
            }
            return View(cAT_Servicio);
        }

        // POST: /Servicio/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="IdServicio,ServicioDescripcion,Activo,OrdenAparicion")] CAT_Servicio cAT_Servicio)
        {
            CAT_Servicio objeto;

            if (ModelState.IsValid)
            {
                objeto = db.CAT_Servicio.Find(cAT_Servicio.IdServicio);

                objeto.ServicioDescripcion = cAT_Servicio.ServicioDescripcion;
                objeto.Activo = cAT_Servicio.Activo;
                objeto.OrdenAparicion = cAT_Servicio.OrdenAparicion;

                objeto.FechaUltimaModificacion = DateTime.Now;
                objeto.UsuarioUltimaModificacion = WebSecurity.CurrentUserName;


                db.Entry(objeto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cAT_Servicio);
        }

        // GET: /Servicio/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CAT_Servicio cAT_Servicio = db.CAT_Servicio.Find(id);
            if (cAT_Servicio == null)
            {
                return HttpNotFound();
            }
            return View(cAT_Servicio);
        }

        // POST: /Servicio/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                CAT_Servicio cAT_Servicio = db.CAT_Servicio.Find(id);
                db.CAT_Servicio.Remove(cAT_Servicio);
                db.SaveChanges();
            }
            catch (Exception)
            {

            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
