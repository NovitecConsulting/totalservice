﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TotalService.Models;

namespace TotalService.Controllers
{
    [Authorize]
    public class MenuController : Controller
    {
        private grocusmx_totalserviceEntities db = new grocusmx_totalserviceEntities();

        // GET: /Menu/
        public ActionResult Index()
        {
            var cat_menu = db.CAT_Menu.Include(c => c.CAT_Aplicacion).Include(c => c.CAT_Menu2);
            return View(cat_menu.ToList());
        }

        // GET: /Menu/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CAT_Menu cAT_Menu = db.CAT_Menu.Find(id);
            if (cAT_Menu == null)
            {
                return HttpNotFound();
            }
            return View(cAT_Menu);
        }

        // GET: /Menu/Create
        public ActionResult Create()
        {
            ViewBag.IdAplicacion = new SelectList(db.CAT_Aplicacion, "IdAplicacion", "Nombre");
            ViewBag.IdMenuPadre = new SelectList(db.CAT_Menu, "IdMenu", "Descripcion");
            return View();
        }

        // POST: /Menu/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="IdMenu,IdMenuPadre,IdAplicacion,Descripcion,URL,Orden,Activo,FechaCreacion,FechaUltimaModificacion,UsuarioCreacion,UsuarioUltimaModificacion")] CAT_Menu cAT_Menu)
        {
            if (ModelState.IsValid)
            {
                db.CAT_Menu.Add(cAT_Menu);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdAplicacion = new SelectList(db.CAT_Aplicacion, "IdAplicacion", "Nombre", cAT_Menu.IdAplicacion);
            ViewBag.IdMenuPadre = new SelectList(db.CAT_Menu, "IdMenu", "Descripcion", cAT_Menu.IdMenuPadre);
            return View(cAT_Menu);
        }

        // GET: /Menu/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CAT_Menu cAT_Menu = db.CAT_Menu.Find(id);
            if (cAT_Menu == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdAplicacion = new SelectList(db.CAT_Aplicacion, "IdAplicacion", "Nombre", cAT_Menu.IdAplicacion);
            ViewBag.IdMenuPadre = new SelectList(db.CAT_Menu, "IdMenu", "Descripcion", cAT_Menu.IdMenuPadre);
            return View(cAT_Menu);
        }

        // POST: /Menu/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="IdMenu,IdMenuPadre,IdAplicacion,Descripcion,URL,Orden,Activo,FechaCreacion,FechaUltimaModificacion,UsuarioCreacion,UsuarioUltimaModificacion")] CAT_Menu cAT_Menu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cAT_Menu).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdAplicacion = new SelectList(db.CAT_Aplicacion, "IdAplicacion", "Nombre", cAT_Menu.IdAplicacion);
            ViewBag.IdMenuPadre = new SelectList(db.CAT_Menu, "IdMenu", "Descripcion", cAT_Menu.IdMenuPadre);
            return View(cAT_Menu);
        }

        // GET: /Menu/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CAT_Menu cAT_Menu = db.CAT_Menu.Find(id);
            if (cAT_Menu == null)
            {
                return HttpNotFound();
            }
            return View(cAT_Menu);
        }

        // POST: /Menu/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CAT_Menu cAT_Menu = db.CAT_Menu.Find(id);
            db.CAT_Menu.Remove(cAT_Menu);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
