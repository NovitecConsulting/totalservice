﻿using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TotalService.Models;
using WebMatrix.WebData;

namespace TotalService.Controllers
{

    public class marker
    {
        public static Random Random = new Random();

        public int Indice { get; set; }

        public decimal Latitud { get; set; }

        public decimal Longitud { get; set; }

        public string NombreMapa { get; set; }
    }

    [Authorize(Roles = Helpers.Comunes.ROL_Administrador + "," + Helpers.Comunes.ROL_Supervisor + "," + Helpers.Comunes.ROL_Operador)]
    public class RutaController : Controller
    {
        private grocusmx_totalserviceEntities db = new grocusmx_totalserviceEntities();

        // GET: /Ruta/
        public ActionResult Index()
        {
            var ope_ruta = db.OPE_Ruta.OrderByDescending(o => o.FechaRuta);

            ViewBag.UsernameTecnico = new SelectList(db.CAT_Usuario, "Username", "Nombre");

            return View(ope_ruta.ToList());
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(OPE_Ruta ruta)
        {
            var ope_ruta = 
                db.OPE_Ruta
                .Where(m => m.UsernameTecnico == ruta.UsernameTecnico || string.IsNullOrEmpty(ruta.UsernameTecnico))
                .Where(m => m.FechaRuta == ruta.FechaRuta || ruta.FechaRuta == null || ruta.FechaRuta == DateTime.MinValue)
                .OrderByDescending(o => o.FechaRuta);

            ViewBag.UsernameTecnico = new SelectList(db.CAT_Usuario, "Username", "Nombre");

            return View("Index", ope_ruta.ToList());

        }

        // GET: /Ruta/Details/5
        public ActionResult Details(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            OPE_Ruta oPE_Ruta = null;

            try
            {

                oPE_Ruta =
                    db.OPE_Ruta
                    .Include(o => o.OPE_Visita.Select(v => v.OPE_Visitado))
                    .Include(o => o.OPE_Visita.Select(v => v.OPE_ServicioVisita.Select(s => s.CAT_Servicio)))
                    .Include(o => o.OPE_Visita.Select(v => v.OPE_Devolucion))
                    .Include(o => o.OPE_Visita.Select(v => v.OPE_CheckOut))
                    .Include(o => o.OPE_Visita.Select(v => v.OPE_CheckOutSinValidar))
                    .Where(o => o.IdRuta == id)
                    .Single();

                if (oPE_Ruta == null)
                {
                    return HttpNotFound();
                }


                foreach (OPE_Visita visita in oPE_Ruta.OPE_Visita)
                {

                    if (visita.OPE_ServicioVisita != null)
                        foreach (OPE_ServicioVisita servicioVisita in visita.OPE_ServicioVisita)
                        {
                            if (string.IsNullOrEmpty(servicioVisita.RutaFotografia) && !string.IsNullOrEmpty(servicioVisita.CadenaFotografia))
                            {
                                servicioVisita.RutaFotografia = ConvierteBase64(servicioVisita.CadenaFotografia);
                                db.Entry(servicioVisita).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                        }

                    if (visita.OPE_Devolucion != null)
                        foreach (OPE_Devolucion devolucion in visita.OPE_Devolucion)
                        {
                            if (string.IsNullOrEmpty(devolucion.RutaFotografia) && !string.IsNullOrEmpty(devolucion.CadenaFotografia))
                            {
                                devolucion.RutaFotografia = ConvierteBase64(devolucion.CadenaFotografia);
                                db.Entry(devolucion).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                        }

                    if (visita.OPE_CheckOut != null)
                    {
                        if (string.IsNullOrEmpty(visita.OPE_CheckOut.RutaFotografiaAtras) && !string.IsNullOrEmpty(visita.OPE_CheckOut.CadenaFotografiaAtras))
                            visita.OPE_CheckOut.RutaFotografiaAtras = ConvierteBase64(visita.OPE_CheckOut.CadenaFotografiaAtras);

                        if (string.IsNullOrEmpty(visita.OPE_CheckOut.RutaFotografiaFrente) && !string.IsNullOrEmpty(visita.OPE_CheckOut.CadenaFotografiaFrente))
                            visita.OPE_CheckOut.RutaFotografiaFrente = ConvierteBase64(visita.OPE_CheckOut.CadenaFotografiaFrente);

                        if (string.IsNullOrEmpty(visita.OPE_CheckOut.RutaFotografiaLateralDerecho) && !string.IsNullOrEmpty(visita.OPE_CheckOut.CadenaFotografiaLateralDerecha))
                            visita.OPE_CheckOut.RutaFotografiaLateralDerecho = ConvierteBase64(visita.OPE_CheckOut.CadenaFotografiaLateralDerecha);

                        if (string.IsNullOrEmpty(visita.OPE_CheckOut.RutaFotografiaLateralIzquierda) && !string.IsNullOrEmpty(visita.OPE_CheckOut.CadenaFotografiaLateralIzquierda))
                            visita.OPE_CheckOut.RutaFotografiaLateralIzquierda = ConvierteBase64(visita.OPE_CheckOut.CadenaFotografiaLateralIzquierda);

                        if (string.IsNullOrEmpty(visita.OPE_CheckOut.RutaFirmaResponsable) && !string.IsNullOrEmpty(visita.OPE_CheckOut.CadenaFirmaResponsable))
                            visita.OPE_CheckOut.RutaFirmaResponsable = ConvierteBase64(visita.OPE_CheckOut.CadenaFirmaResponsable);

                        db.Entry(visita.OPE_CheckOut).State = EntityState.Modified;
                        db.SaveChanges();

                    }


                    if (visita.OPE_CheckOutSinValidar != null)
                    {
                        if (string.IsNullOrEmpty(visita.OPE_CheckOutSinValidar.RutaFotografiaAtras) && !string.IsNullOrEmpty(visita.OPE_CheckOutSinValidar.CadenaFotografiaAtras))
                            visita.OPE_CheckOutSinValidar.RutaFotografiaAtras = ConvierteBase64(visita.OPE_CheckOutSinValidar.CadenaFotografiaAtras);

                        if (string.IsNullOrEmpty(visita.OPE_CheckOutSinValidar.RutaFotografiaFrente) && !string.IsNullOrEmpty(visita.OPE_CheckOutSinValidar.CadenaFotografiaFrente))
                            visita.OPE_CheckOutSinValidar.RutaFotografiaFrente = ConvierteBase64(visita.OPE_CheckOutSinValidar.CadenaFotografiaFrente);

                        if (string.IsNullOrEmpty(visita.OPE_CheckOutSinValidar.RutaFotografiaLateralDerecho) && !string.IsNullOrEmpty(visita.OPE_CheckOutSinValidar.CadenaFotografiaLateralDerecha))
                            visita.OPE_CheckOutSinValidar.RutaFotografiaLateralDerecho = ConvierteBase64(visita.OPE_CheckOutSinValidar.CadenaFotografiaLateralDerecha);

                        if (string.IsNullOrEmpty(visita.OPE_CheckOutSinValidar.RutaFotografiaLateralIzquierda) && !string.IsNullOrEmpty(visita.OPE_CheckOutSinValidar.CadenaFotografiaLateralIzquierda))
                            visita.OPE_CheckOutSinValidar.RutaFotografiaLateralIzquierda = ConvierteBase64(visita.OPE_CheckOutSinValidar.CadenaFotografiaLateralIzquierda);


                        db.Entry(visita.OPE_CheckOutSinValidar).State = EntityState.Modified;
                        db.SaveChanges();

                    }

                }

            }
            catch (DbEntityValidationException ex)
            {
                foreach (DbEntityValidationResult item2 in ex.EntityValidationErrors)
                {
                    // Get entry

                    DbEntityEntry entry = item2.Entry;
                    string entityTypeName = entry.Entity.GetType().Name;

                    // Display or log error messages

                    foreach (DbValidationError subItem in item2.ValidationErrors)
                    {
                        string message = string.Format("Error '{0}' occurred in {1} at {2}",
                                 subItem.ErrorMessage, entityTypeName, subItem.PropertyName);
                        Console.WriteLine(message);
                    }
                }
            }

            return View(oPE_Ruta);
        }

        private string ConvierteBase64(string base64)
        {
            string ruta = string.Empty;
            string archivo = Guid.NewGuid().ToString("N");

            try
            {
                byte[] imageBytes = Convert.FromBase64String(base64);
                
                ruta = Path.Combine(Server.MapPath("~/Imagenes/"), string.Format("{0}.jpeg", archivo));

                using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
                {
                    Image image = Image.FromStream(ms, true);
                    image.Save(ruta, System.Drawing.Imaging.ImageFormat.Jpeg);

                }
            }
            catch (Exception ex)
            {
            }

            return ruta;

        }



        // GET: /Ruta/Create
        public ActionResult Create()
        {
            ViewBag.UsernameTecnico = new SelectList(db.CAT_Usuario.Where(m => m.Activo), "Username", "Nombre");
            Session["Listado"] = null;

            return View();
        }

        // POST: /Ruta/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(OPE_Ruta oPE_Ruta)
        {
            List<OPE_Visita> listaVisita = null;
            OPE_EncuestaVisita encuesta = new OPE_EncuestaVisita();
            OPE_Visita visita;

            if (Session["Listado"] != null)
            {
                listaVisita = (List<OPE_Visita>)Session["Listado"];
            }

            if (ModelState.IsValid && listaVisita != null && listaVisita.Count > 0)
            {
                oPE_Ruta.FechaCreacion = DateTime.Now;
                oPE_Ruta.UsuarioCreacion = WebSecurity.CurrentUserName;

                oPE_Ruta.FechaUltimaModificacion = DateTime.Now;
                oPE_Ruta.UsuarioUltimaModificacion = WebSecurity.CurrentUserName;

                db.OPE_Ruta.Add(oPE_Ruta);
                db.SaveChanges();



                foreach (OPE_Visita item in listaVisita)
                {
                    item.OPE_Visitado.FechaCreacion = DateTime.Now;
                    item.OPE_Visitado.UsuarioCreacion = WebSecurity.CurrentUserName;
                    item.OPE_Visitado.Editable = true;
                    item.OPE_Visitado.FechaUltimaModificacion = DateTime.Now;
                    item.OPE_Visitado.UsuarioUltimaModificacion = WebSecurity.CurrentUserName;

                    db.OPE_Visitado.Add(item.OPE_Visitado);
                    db.SaveChanges();



                    visita = new OPE_Visita();

                    visita.IdRuta = oPE_Ruta.IdRuta;
                    visita.IdVisitado = item.OPE_Visitado.IdVisitado;
                    visita.IdEstatusVisita = 4;

                    visita.FechaCreacion = DateTime.Now;
                    visita.UsuarioCreacion = WebSecurity.CurrentUserName;

                    visita.FechaUltimaModificacion = DateTime.Now;
                    visita.UsuarioUltimaModificacion = WebSecurity.CurrentUserName;
                    db.OPE_Visita.Add(visita);
                    db.SaveChanges();

                    encuesta.FechaCreacion = DateTime.Now;
                    encuesta.FechaEncuesta = DateTime.Now;
                    //encuesta.IdEncuesta = 1;
                    encuesta.IdVisita = visita.IdVisita;
                    db.SaveChanges();
                }


                return RedirectToAction("Index");
            }

            ViewBag.UsernameTecnico = new SelectList(db.CAT_Usuario, "Username", "Nombre", oPE_Ruta.UsernameTecnico);
            return View(oPE_Ruta);
        }

        // GET: /Ruta/Edit/5
        public ActionResult Edit(int? id)
        {
            List<OPE_Visita> listaVisita;
            List<OPE_Visita> listaVisita2;
       
            OPE_Ruta ope_ruta = null;


            Session["ListadoEdicion"] = null;
            Session["Listado"] = null;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            try
            {
                ope_ruta =
                        db.OPE_Ruta
                        .Include(o => o.OPE_Visita.Select(v => v.OPE_Visitado))
                        .Where(o => o.IdRuta == id)
                        .Single();

                if (ope_ruta == null)
                {
                    return HttpNotFound();
                }


                listaVisita = new List<OPE_Visita>();
                listaVisita2 = new List<OPE_Visita>();
                
                foreach (OPE_Visita itemVisita in ope_ruta.OPE_Visita)
                {
                    //itemVisita.OPE_Visitado.Editable = string.IsNullOrEmpty(itemVisita.LatitudCheckIn);

                    listaVisita.Add(itemVisita);
                    listaVisita2.Add(itemVisita);
                }
                
                Session["ListadoEdicion"] = listaVisita;
                Session["Listado"] = listaVisita2;


                ViewBag.UsernameTecnico = new SelectList(db.CAT_Usuario, "Username", "Nombre", ope_ruta.UsernameTecnico);
            }
            catch (Exception ex)
            {

            }

            return View(ope_ruta);
        }

        // POST: /Ruta/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(OPE_Ruta oPE_Ruta)
        {
            List<OPE_Visita> listaVisita = new List<OPE_Visita>();
            List<OPE_Visita> listaVisita2 = new List<OPE_Visita>();
            bool modificado;
            OPE_Ruta objeto;
            OPE_Visita visita;
            OPE_Visitado visitado;

            if (ModelState.IsValid)
            {
                if (Session["Listado"] != null)
                    listaVisita = (List<OPE_Visita>)Session["Listado"];

                if (Session["ListadoEdicion"] != null)
                    listaVisita2 = (List<OPE_Visita>)Session["ListadoEdicion"];

                try
                {

                    //db.Database.BeginTransaction();

                    

                    modificado = false;
                    if (listaVisita2.Count != listaVisita.Count)
                        modificado = true;
                    else
                    {
                        for (int i = 0; i < listaVisita.Count; i++)
                            if (listaVisita[i].IdVisitado != listaVisita2[i].IdVisitado)
                            {
                                modificado = true;
                                break;
                            }
                    }
                    

                    if (modificado)
                    {

                        foreach (OPE_Visita item in listaVisita2)
                            if (item.OPE_Visitado.Editable.Value)
                            {

                                visita = new OPE_Visita();
                                visita.IdVisita = item.IdVisita;
                                db.OPE_Visita.Attach(visita);
                                db.OPE_Visita.Remove(visita);
                                /*
                                item.IdEstatusVisita = 13;
                                db.Entry(item).State = EntityState.Modified;*/
                            }

                        db.SaveChanges();

                        foreach (OPE_Visita item in listaVisita)
                        {
                            if (item.OPE_Visitado.Editable.Value)
                            {
                                visitado = new OPE_Visitado();
                                visitado.Colonia = item.OPE_Visitado.Colonia;
                                visitado.Direccion = item.OPE_Visitado.Direccion;
                                visitado.Municipio = item.OPE_Visitado.Municipio;
                                visitado.NombreEmpresa = item.OPE_Visitado.NombreEmpresa;
                                visitado.NombreResponsable = item.OPE_Visitado.NombreResponsable;
                                visitado.Telefono = item.OPE_Visitado.Telefono;
                              
                                visitado.FechaCreacion = DateTime.Now;
                                visitado.UsuarioCreacion = WebSecurity.CurrentUserName;

                                visitado.FechaUltimaModificacion = DateTime.Now;
                                visitado.UsuarioUltimaModificacion = WebSecurity.CurrentUserName;

                                db.OPE_Visitado.Add(visitado);

                                db.SaveChanges();

                                visita = new OPE_Visita();

                                visita.IdRuta = oPE_Ruta.IdRuta;
                                visita.IdVisitado = visitado.IdVisitado;
                                visita.IdEstatusVisita = 4;

                                visita.FechaCreacion = DateTime.Now;
                                visita.UsuarioCreacion = WebSecurity.CurrentUserName;

                                visita.FechaUltimaModificacion = DateTime.Now;
                                visita.UsuarioUltimaModificacion = WebSecurity.CurrentUserName;

                                db.OPE_Visita.Add(visita);

                                db.SaveChanges();

                            }
                        }
                        /*
                        foreach (CAT_Usuario usuario in db.CAT_Usuario.Local)
                            db.Entry(usuario).State = EntityState.Unchanged;
                        */
    

                    }

                    objeto = db.OPE_Ruta.Find(oPE_Ruta.IdRuta);

                    objeto.UsernameTecnico = oPE_Ruta.UsernameTecnico;
                    objeto.FechaRuta = oPE_Ruta.FechaRuta;

                    objeto.FechaUltimaModificacion = DateTime.Now;
                    objeto.UsuarioUltimaModificacion = WebSecurity.CurrentUserName;

                    db.Entry(objeto).State = EntityState.Modified;
                    db.SaveChanges();



                    ///db.Database.CurrentTransaction.Commit();
                }
                catch (DbEntityValidationException ex)
                {
                    //db.Database.CurrentTransaction.Rollback();

                    foreach (DbEntityValidationResult item2 in ex.EntityValidationErrors)
                    {
                        // Get entry

                        DbEntityEntry entry = item2.Entry;
                        string entityTypeName = entry.Entity.GetType().Name;

                        // Display or log error messages

                        foreach (DbValidationError subItem in item2.ValidationErrors)
                        {
                            string message = string.Format("Error '{0}' occurred in {1} at {2}",
                                     subItem.ErrorMessage, entityTypeName, subItem.PropertyName);
                            Console.WriteLine(message);
                        }
                    }
                }

                return RedirectToAction("Index");

            }

            ViewBag.UsernameTecnico = new SelectList(db.CAT_Usuario, "Username", "Nombre", oPE_Ruta.UsernameTecnico);

            return View(oPE_Ruta);
        }

        // GET: /Ruta/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OPE_Ruta oPE_Ruta = db.OPE_Ruta.Find(id);
            if (oPE_Ruta == null)
            {
                return HttpNotFound();
            }
            return View(oPE_Ruta);
        }

        // POST: /Ruta/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OPE_Ruta oPE_Ruta = db.OPE_Ruta.Find(id);
            db.OPE_Ruta.Remove(oPE_Ruta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }



        // POST: 
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Visitado(OPE_Visitado oPE_Visitado)
        {
            List<OPE_Visita> visita;

            if (ModelState.IsValid)
            {

                if (Session["Listado"] == null)
                {
                    visita = new List<OPE_Visita>();
                    Session["Listado"] = visita;
                }
                else
                {
                    visita = (List<OPE_Visita>)Session["Listado"];
                }


                visita.Add(new OPE_Visita()
                {
                    OPE_Visitado = new OPE_Visitado()
                    {
                        NombreEmpresa = oPE_Visitado.NombreEmpresa,
                        NombreResponsable = oPE_Visitado.NombreResponsable,
                        Editable = true,
                        Direccion = oPE_Visitado.Direccion,
                        Colonia = oPE_Visitado.Colonia,
                        Municipio = oPE_Visitado.Municipio,
                        Telefono = oPE_Visitado.Telefono
                    }
                });
                
            }

            return PartialView("_IndexVisitado", Session["Listado"]);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult EliminaVisitado(FormCollection formCollection)
        {
            List<OPE_Visita> visitado;

            int indice;

            if (!string.IsNullOrEmpty(formCollection["filaEliminar"]) && int.TryParse(formCollection["filaEliminar"], out indice))
            {

                if (Session["Listado"] == null)
                {
                    visitado = new List<OPE_Visita>();
                    Session["Listado"] = visitado;
                }
                else
                {
                    visitado = (List<OPE_Visita>)Session["Listado"];
                    if (indice >= 0 && indice < visitado.Count)
                    {
                        visitado.RemoveAt(indice);
                    }
                }
            }


            return PartialView("_IndexVisitado", Session["Listado"]);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
