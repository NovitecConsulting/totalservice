﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using TotalService.Filters;
using TotalService.Models;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;

namespace TotalService.Controllers
{
 
    [InitializeSimpleMembership]
    public class AccountController : Controller
    {
        private grocusmx_totalserviceEntities db = new grocusmx_totalserviceEntities();

        //
        // GET: /Account/Login

        UsersContext context = new UsersContext();

        [Authorize(Roles = Helpers.Comunes.ROL_Administrador + "," + Helpers.Comunes.ROL_Supervisor)]
        public ActionResult Index()
        {
            return View(context.UserProfiles.ToList());
        }


        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid && WebSecurity.Login(model.UserName,model.Password))
            {
                Session["username"] = model.UserName;
                if (string.IsNullOrEmpty(returnUrl))
                    return RedirectToAction("Index", "Ruta");
                else
                    return RedirectToLocal(returnUrl);
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "The user name or password provided is incorrect.");
            return View(model);
        }

        //
        // POST: /Account/LogOff

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            WebSecurity.Logout();

            return RedirectToAction("Login", "Account");
        }
        

        //
        // GET: /Account/Register

        [AllowAnonymous]
        [Authorize(Roles = Helpers.Comunes.ROL_Administrador + "," + Helpers.Comunes.ROL_Supervisor)]
        public ActionResult Register()
        {
            
            ViewBag.Name = new SelectList(Roles.GetAllRoles()); 

            return View();
        }

        //
        // POST: /Account/Register

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = Helpers.Comunes.ROL_Administrador + "," + Helpers.Comunes.ROL_Supervisor)]
        public ActionResult Register(RegisterModel model)
        {
            CAT_Usuario usuario;

            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    WebSecurity.CreateUserAndAccount(model.UserName, model.Password, new { FullName = model.FullName});

                   Roles.AddUserToRole(model.UserName, model.UserRoles);

                    usuario = new CAT_Usuario();

                    usuario.Activo = true;
                    usuario.FechaCreacion = DateTime.Now;
                    usuario.UsuarioCreacion = WebSecurity.CurrentUserName;
                    usuario.FechaUltimaModificacion = DateTime.Now;
                    usuario.UsuarioUltimaModificacion =  WebSecurity.CurrentUserName;
                    usuario.Username = model.UserName;
                    usuario.Nombre = model.FullName;
                    usuario.Apellidos = " ";

                    db.CAT_Usuario.Add(usuario);
                    db.SaveChanges();

                    
                    return RedirectToAction("Index", "Account");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
                catch (DbEntityValidationException ex)
                {
                    //db.Database.CurrentTransaction.Rollback();

                    foreach (DbEntityValidationResult item2 in ex.EntityValidationErrors)
                    {
                        // Get entry

                        DbEntityEntry entry = item2.Entry;
                        string entityTypeName = entry.Entity.GetType().Name;

                        // Display or log error messages

                        foreach (DbValidationError subItem in item2.ValidationErrors)
                        {
                            string message = string.Format("Error '{0}' occurred in {1} at {2}",
                                     subItem.ErrorMessage, entityTypeName, subItem.PropertyName);
                            Console.WriteLine(message);
                        }
                    }
                }
        }

            // If we got this far, something failed, redisplay form
            return View(model);
        }


        //
        // GET: /Account/Register

        [AllowAnonymous]
        [Authorize(Roles = Helpers.Comunes.ROL_Administrador + "," + Helpers.Comunes.ROL_Supervisor)]
        public ActionResult Edit(int userId)
        {
            // Fetch the userprofile
            UserProfile user = context.UserProfiles.FirstOrDefault(u => u.UserId == userId);
            
            // Construct the viewmodel
            EditModel model = new EditModel();

            model.UserId = user.UserId;
            model.FullName = user.FullName;
            model.UserName = user.UserName;
            model.UserRoles = Roles.GetRolesForUser(user.UserName)[0];
            
            ViewBag.Name = new SelectList(Roles.GetAllRoles());

            return View(model);
            
        }

        //
        // POST: /Account/Register

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = Helpers.Comunes.ROL_Administrador + "," + Helpers.Comunes.ROL_Supervisor)]
        public ActionResult Edit(EditModel model, int Activo)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    using (grocusmx_totalserviceEntities context = new grocusmx_totalserviceEntities())
                    {
                        CAT_Usuario usuario = context.CAT_Usuario.Find(model.UserName);
                        usuario.Activo = Convert.ToBoolean(Activo);
                        context.Entry(usuario).State = System.Data.Entity.EntityState.Modified;
                        context.SaveChanges();
                    }

                    UserProfile user = context.UserProfiles.FirstOrDefault(u => u.UserId == model.UserId);
                   
                    user.FullName = model.FullName;
                    //user.UserName = model.UserName;

                    context.Entry(user).State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges();

                    if (Roles.GetRolesForUser(model.UserName)[0].CompareTo(model.UserRoles) != 0)
                    {
                        Roles.RemoveUserFromRoles(user.UserName, new string[] { Roles.GetRolesForUser(user.UserName)[0] });
                        Roles.AddUserToRole(model.UserName, model.UserRoles);
                    }
                    
                    return RedirectToAction("Index", "Account");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }
           
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/Disassociate

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Disassociate(string provider, string providerUserId)
        {
            string ownerAccount = OAuthWebSecurity.GetUserName(provider, providerUserId);
            ManageMessageId? message = null;

            // Only disassociate the account if the currently logged in user is the owner
            if (ownerAccount == User.Identity.Name)
            {
                // Use a transaction to prevent the user from deleting their last login credential
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
                {
                    bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
                    if (hasLocalAccount || OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name).Count > 1)
                    {
                        OAuthWebSecurity.DeleteAccount(provider, providerUserId);
                        scope.Complete();
                        message = ManageMessageId.RemoveLoginSuccess;
                    }
                }
            }

            return RedirectToAction("Manage", new { Message = message });
        }

        //
        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ChangePassword(string userName, string oldPassword, string newPassword)
        {
            try
            {
                WebSecurity.ChangePassword(userName, oldPassword, newPassword);
                ViewBag.Mensaje = "Su contraseña se ha cambiado correctamente";
            }
            catch (Exception)
            {

            }
            return View("ChangePassword");
        }
        // GET: /Account/Manage
        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : "";
            ViewBag.HasLocalPassword = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(LocalPasswordModel model)
        {
            bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.HasLocalPassword = hasLocalAccount;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasLocalAccount)
            {
                if (ModelState.IsValid)
                {
                    // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                    bool changePasswordSucceeded;
                    try
                    {
                        changePasswordSucceeded = WebSecurity.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword);
                    }
                    catch (Exception)
                    {
                        changePasswordSucceeded = false;
                    }

                    if (changePasswordSucceeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                    }
                }
            }
            else
            {
                // User does not have a local password so remove any validation errors caused by a missing
                // OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        WebSecurity.CreateAccount(User.Identity.Name, model.NewPassword);
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError("", e);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }


        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
