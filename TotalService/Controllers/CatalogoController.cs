﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TotalService.Models;
using WebMatrix.WebData;

namespace TotalService.Controllers
{

    //[Authorize(Roles = Helpers.Comunes.ROL_Administrador + "," + Helpers.Comunes.ROL_Supervisor)]
    public class CatalogoController : Controller
    {
        private grocusmx_totalserviceEntities db = new grocusmx_totalserviceEntities();

        // GET: /Catalogo/
        public ActionResult Index()
        {
            var cat_catalogo = db.CAT_Catalogo.Include(c => c.CAT_TipoCatalogo).Where(cat => cat.CAT_TipoCatalogo.TipoCatalogo == "MotivoCheckOutSinValidar");
            return View(cat_catalogo.ToList());
        }

        // GET: /Catalogo/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CAT_Catalogo cAT_Catalogo = db.CAT_Catalogo.Find(id);
            if (cAT_Catalogo == null)
            {
                return HttpNotFound();
            }
            return View(cAT_Catalogo);
        }

        // GET: /Catalogo/Create
        public ActionResult Create()
        {
            ViewBag.IdTipoCatalogo = new SelectList(db.CAT_TipoCatalogo, "IdTipoCatalogo", "TipoCatalogo");
            return View();
        }

        // POST: /Catalogo/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="IdCatalogo,IdTipoCatalogo,CatalogoDescripcion,OrdenAparicion,Activo,UsuarioCreacion,UsuarioUltimaModificacion,FechaCreacion,FechaUltimaModificacion")] CAT_Catalogo cAT_Catalogo)
        {
            if (ModelState.IsValid)
            {
                cAT_Catalogo.FechaCreacion = DateTime.Now;
                cAT_Catalogo.UsuarioCreacion = WebSecurity.CurrentUserName;

                cAT_Catalogo.FechaUltimaModificacion = DateTime.Now;
                cAT_Catalogo.UsuarioUltimaModificacion = WebSecurity.CurrentUserName;


                db.CAT_Catalogo.Add(cAT_Catalogo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdTipoCatalogo = new SelectList(db.CAT_TipoCatalogo, "IdTipoCatalogo", "TipoCatalogo", cAT_Catalogo.IdTipoCatalogo);
            return View(cAT_Catalogo);
        }

        // GET: /Catalogo/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CAT_Catalogo cAT_Catalogo = db.CAT_Catalogo.Find(id);
            if (cAT_Catalogo == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdTipoCatalogo = new SelectList(db.CAT_TipoCatalogo, "IdTipoCatalogo", "TipoCatalogo", cAT_Catalogo.IdTipoCatalogo);
            return View(cAT_Catalogo);
        }

        // POST: /Catalogo/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="IdCatalogo,IdTipoCatalogo,CatalogoDescripcion,OrdenAparicion,Activo,UsuarioCreacion,UsuarioUltimaModificacion,FechaCreacion,FechaUltimaModificacion")] CAT_Catalogo cAT_Catalogo)
        {
            CAT_Catalogo objeto;

            if (ModelState.IsValid)
            {
                objeto = db.CAT_Catalogo.Find(cAT_Catalogo.IdCatalogo);
                
                objeto.FechaUltimaModificacion = DateTime.Now;
                objeto.UsuarioUltimaModificacion = WebSecurity.CurrentUserName;

                objeto.CatalogoDescripcion = cAT_Catalogo.CatalogoDescripcion;
                objeto.OrdenAparicion = cAT_Catalogo.OrdenAparicion;
                objeto.Activo = cAT_Catalogo.Activo;
                objeto.IdTipoCatalogo = cAT_Catalogo.IdTipoCatalogo;

                db.Entry(objeto).State = EntityState.Modified;

                db.SaveChanges();

                
                return RedirectToAction("Index");
            }
            ViewBag.IdTipoCatalogo = new SelectList(db.CAT_TipoCatalogo, "IdTipoCatalogo", "TipoCatalogo", cAT_Catalogo.IdTipoCatalogo);
            return View(cAT_Catalogo);
        }

        // GET: /Catalogo/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CAT_Catalogo cAT_Catalogo = db.CAT_Catalogo.Find(id);
            if (cAT_Catalogo == null)
            {
                return HttpNotFound();
            }
            return View(cAT_Catalogo);
        }

        // POST: /Catalogo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CAT_Catalogo cAT_Catalogo = db.CAT_Catalogo.Find(id);
            db.CAT_Catalogo.Remove(cAT_Catalogo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
