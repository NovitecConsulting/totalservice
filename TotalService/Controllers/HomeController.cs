﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TotalService.Controllers
{

    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ObtenerResultadosGrafica(DateTime.Now, new DateTime(DateTime.Now.Year, 12, 31));
         
            List<Grafica> grafica = new List<Grafica>();
            grafica.Add(new Grafica { CantidadVisitas = 3, Tecnico = "usuario", Mes = "Abril" });
            ViewBag.Grafica = grafica;
            return View();
        }

        [Authorize(Roles ="Administrador")]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public void ObtenerResultadosGrafica(DateTime inicio, DateTime fin)
        {

            List<Grafica> grafica = new List<Grafica>();
            SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            try
            {
                if (cnn.State == System.Data.ConnectionState.Open)
                {
                    cnn.Close();
                }
                cnn.Open();
                using (SqlCommand command = new SqlCommand("[grocusmx_totalse].[spObtenerTotalVisitasUsuarioMes]", cnn))
                {
                    command.Parameters.AddWithValue("@FechaInicio", inicio); 
                    command.Parameters.AddWithValue("@FechaFin", fin);
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                grafica.Add(new Grafica { CantidadVisitas = reader.GetInt32(0), Tecnico = reader.GetString(1), Mes = reader.GetString(2) });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (cnn.State != System.Data.ConnectionState.Closed)
                {
                    cnn.Close(); 
                }

                ViewBag.Grafica = grafica;
            }
        }
    }

    public class Grafica
    {
        public int CantidadVisitas { get; set; }
        public string Tecnico { get; set; }
        public string Mes { get; set; }
    }
}