﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReporteEvidencia.aspx.cs" Inherits="TotalService.ReporteEvidencia" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
        #contenedor, #Usuario, .boton{
            margin:1em;
        }
       
        .txtUsuario{
            width:350px;
            display:inline;
        }
        #calendarios{
            display:block;
        }
        .calendario{
            display:inline;
        }
        

    </style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Reporte de evidencias</title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="contenedor">
        <div id="Usuario"><asp:Label runat="server">Usuario: </asp:Label>
        <asp:TextBox ID="txtUsuario" runat="server" CssClass="txtUsuario"></asp:TextBox>

        </div>
        
        <div id="calendarios">
        <asp:Label runat="server">Fecha Inicio: </asp:Label>
        <asp:Calendar ID="Calendar1" runat="server" CssClass="calendario"></asp:Calendar>
            <asp:Label runat="server">Fecha Final: </asp:Label>
             <asp:Calendar ID="Calendar2" runat="server" CssClass="calendario"></asp:Calendar>
        </div>
       
    <asp:Button ID="btn1" runat="server" Text="Generate report" onclick="btn1_Click" CssClass="boton"/>
        <br />
        <br />
        </div>
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%">
             <LocalReport ReportPath="Report1.rdlc">
                
            </LocalReport>
        </rsweb:ReportViewer>

    </div>
    </form>
</body>
</html>
