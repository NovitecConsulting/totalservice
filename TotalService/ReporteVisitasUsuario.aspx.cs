﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TotalService
{
    public partial class ReporteVisitasUsuario : System.Web.UI.Page
    {
        public string thisConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        private DataSet GetData(string fecha, string usuario)
        {
            SqlConnection con = new SqlConnection(thisConnectionString);
            con.Open();
            SqlCommand cmd = new SqlCommand("select visitasProgramadas = count(1) , " +
                                 "visitasCheckOut = count(CASE when idEstatusVisita = 6 then 1 End) , " +
                                 "visitasCheckOutSinValida = count(CASE when idEstatusVisita = 7 then 1 End) , " +
                                 "visitasSinRealizar = count(CASE when idEstatusVisita = 4 or idestatusVisita = 8 then 1 End) , " +
                                 "r.userNameTecnico " +
                                 "from OPe_Visita as v " +
                                 "inner join ope_ruta r on v.idRuta = r.idRuta " +
                                 "where Year(fechaRuta) =  Year('" + fecha + "') " +
                                 " and (Month(fechaRuta) = Month('" + fecha + "') " +
                                 " Or '" + fecha + "' is null) " +
                                 " and r.Usernametecnico = '" + usuario + "'"+
                                 " group by r.Usernametecnico ");
            using (con)
            {
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return (ds);
                }
            }
        }

        protected void btn1_Click(object sender, EventArgs e)
        {
            ReportViewer1.Visible = true;
            string usuario = txtUsuario.Text;
            string fecha = Calendar1.SelectedDate.ToString("yyyy-MM-dd");
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/ReporteVisitas.rdlc");
            DataSet ds = new DataSet();
            ds = GetData(fecha, usuario);
            if (ds.Tables[0].Rows.Count > 0)
            {
                ReportDataSource rds = new ReportDataSource("dsReporteVisitas", ds.Tables[0]);
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(rds);
            }
            else
            {
                Response.Write("<script>window.alert('No se encuentra informacion');</script>");
            }
        }
    }
}