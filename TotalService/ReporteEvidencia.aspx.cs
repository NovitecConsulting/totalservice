﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TotalService
{
    public partial class ReporteEvidencia : System.Web.UI.Page
    {
        public string thisConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn1_Click(object sender, EventArgs e)
        {
            ReportViewer1.Visible = true;
            string usuario = txtUsuario.Text;
            string fechaInicio = Calendar1.SelectedDate.ToString("yyyy-MM-dd");
            string fechaFin = Calendar2.SelectedDate.ToString("yyyy-MM-dd");

            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Report1.rdlc");
            DataSet ds = new DataSet();
            ds = GetData(fechaInicio, fechaFin,usuario);
            if (ds.Tables[0].Rows.Count > 0)
            {
                ReportDataSource rds = new ReportDataSource("dsReporteEvidencia", ds.Tables[0]);
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(rds);

            }
            else
            {
                Response.Write("<script>window.alert('No se encuentra informacion');</script>");
            }
        }

        private DataSet GetData(string fechaInicio, string fechaFin, string usuario)
        {
            SqlConnection con = new SqlConnection(thisConnectionString);
            con.Open();
            SqlCommand cmd = new SqlCommand("SELECT RUT.[FechaRuta] AS FechaVisita, "+
                 "CONVERT(VARCHAR, RUT.[UsernameTecnico], 113) AS Tecnico, " +
                 "VSTD.[NombreEmpresa] AS NombreEmpresa, "+
                 "VSTD.[NombreResponsable] AS NombreResponsable, "+
                 "CAT.[CatalogoDescripcion] AS EstatusVisita, "+
                 "CATm.[CatalogoDescripcion] AS MotivoCheckOutSinValidar, "+
                 "CHC.[Comentario] AS Comentario, "+
                 "CHC.[CadenaFotografiaFrente] AS CadenaFotoFrente, "+
                 "CHC.[CadenaFotografiaLateralDerecha] AS CadenaFotoDerecha, "+
                 "CHC.[CadenaFotografiaLateralIzquierda] AS CadenaFotoIzquierda, " +
                 "CHC.[CadenaFotografiaAtras] AS RutaFotoAtras "+
                 "FROM [dbo].[OPE_Ruta] RUT JOIN [dbo].[OPE_Visita] VST ON VST.[IdRuta]= RUT.[IdRuta] "+
                 "JOIN [dbo].[OPE_Visitado] VSTD ON   VSTD.[IdVisitado]= VST.[IdVisitado] "+
                 "JOIN [dbo].[CAT_Catalogo] CAT ON CAT.[IdCatalogo] = VST.[IdEstatusVisita] "+
                 "JOIN [dbo].[OPE_CheckOut]CHC ON CHC.[IdCheckOut]= VST.[IdCheckOut] "+
                 "LEFT JOIN [dbo].[OPE_CheckOutSinValidar] CHOS ON CHOS.[IdCheckOutSinValidar]= VST.[IdCheckOutSinValidar] "+
                 "LEFT JOIN [dbo].[CAT_Catalogo] CATm ON CATm.[IdCatalogo] = CHOS.[IdMotivoCheckOutSinValidar] "+
                 "WHERE (RUT.[FechaRuta] BETWEEN '"+fechaInicio+"'"+
                 "and '"+fechaFin+"') and "+
                 "RUT.[UserNameTecnico] = '"+usuario+"' ");
            using (con)
            {
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return (ds);
                }
            }

        }
    }
}