﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TotalService.Models
{
    [Serializable]
    public class PreguntaModelView
    {
        public int idPregunta { get; set; }
        public string DescripcionPregunta { get; set; }
        public string UsuarioCreacion { get; set; }
        public List<RespuestaModelView> listaRespuestas { get; set; }
    }
}