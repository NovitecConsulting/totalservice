//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TotalService.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CAT_Menu
    {
        public CAT_Menu()
        {
            this.CAT_Menu1 = new HashSet<CAT_Menu>();
        }
    
        public int IdMenu { get; set; }
        public Nullable<int> IdMenuPadre { get; set; }
        public Nullable<int> IdAplicacion { get; set; }
        public string Descripcion { get; set; }
        public string URL { get; set; }
        public byte Orden { get; set; }
        public bool Activo { get; set; }
        public System.DateTime FechaCreacion { get; set; }
        public System.DateTime FechaUltimaModificacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioUltimaModificacion { get; set; }
    
        public virtual CAT_Aplicacion CAT_Aplicacion { get; set; }
        public virtual ICollection<CAT_Menu> CAT_Menu1 { get; set; }
        public virtual CAT_Menu CAT_Menu2 { get; set; }
    }
}
