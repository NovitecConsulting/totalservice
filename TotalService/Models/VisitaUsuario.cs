﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TotalService.Models
{
    public class VisitasUsuario

    {
        public DateTime calendario { get; set; }

        public int VisitasProgramadas { get; set; }

        public int VisitasCheckOut { get; set; }

        public int VisitasCheckOutSinValidar { get; set; }

        public int VisitarsSinRealizar { get; set; }

    }
}