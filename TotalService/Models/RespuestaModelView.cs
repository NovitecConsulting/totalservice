﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TotalService.Models
{
    [Serializable]
    public class RespuestaModelView
    {
        public string UsuarioCreacion { get; set; }
        public int idRespuesta { get; set; }
        public string DescripcionRespuesta { get; set; }
    }
}