﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TotalService.Models
{
    public class UsuarioEvidencia
    {

        public string Username { get; set; }

        public string NombreVisitado { get; set; }

        public string NombreResponsableVisitado { get; set; }

        public int IdRuta { get; set; }

        public int IdVisita { get; set; }

        public string EstatusVisita { get; set; }

        public string MotivoCheckOutSinValidar { get; set; }

        public string Comentario { get; set; }

        public string CadenaFotoFrente { get; set; }

        public string RutaFotoFrente { get; set; }

        public string CadenaFotoAtras { get; set; }

        public string RutaFotoAtras { get; set; }

        public string CadenaFotoIzquierda { get; set; }

        public string RutaFotoIzquierda { get; set; }

        public string CadenaFotoDerecha { get; set; }

        public string RutaFotoFrenteDerecha { get; set; }

        public string FirmaResponsable { get; set; }

    }

    /*fecha de la visita, 

     * nombre del técnico,

     *  nombre de la empresa,

     *   responsable, 

     *   estatus de la visita, 

     *   motivo de checkout sin validar,

     *    comentario, 

     *    foto frente, 

     *    foto atrás, 

     *    foto derecha, 

     *    foto izquierda, 

     *    firma del responsable*/
}