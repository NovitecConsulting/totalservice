﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TotalService.Models
{
    [Serializable]
    public class EncuestaModelView
    {
        public int idEncuesta { get; set; }
        public string DescripcionEncuesta { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioUltimaModificacion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public List<PreguntaModelView> listaPreguntas { get; set; }
       
    }
}