﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TotalService.Models
{
    public class ServicioVisitadoViewModel
    {
        
        public OPE_Visitado Visitado { get; set; }

        public List<CAT_Servicio> Servicios { get; set; }

        public ServicioVisitadoViewModel()
        {
            Visitado = null;

            Servicios = new List<CAT_Servicio>();
        }

    }
}