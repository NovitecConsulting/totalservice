﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TotalService.Models
{
    public class RutaVisitado
    {

        public OPE_Ruta Ruta { get; set; }
        
        public OPE_Visitado _Visitado { get; set; }

        public List<OPE_Visitado> Visitado { get; set; }


        public List<OPE_Visita> Visita { get; set; }

        public RutaVisitado()
        {
            Ruta = null;
            _Visitado = null;
            Visita = new List<OPE_Visita>();

            Visitado = new List<OPE_Visitado>();
        }

    }
}