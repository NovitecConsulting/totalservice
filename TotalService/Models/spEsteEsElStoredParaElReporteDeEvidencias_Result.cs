//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TotalService.Models
{
    using System;
    
    public partial class spEsteEsElStoredParaElReporteDeEvidencias_Result
    {
        public string FechaVisita { get; set; }
        public string Tecnico { get; set; }
        public string NombreEmpresa { get; set; }
        public string NombreResponsable { get; set; }
        public string EstatusVisita { get; set; }
        public string MotivoCheckOutSinValidar { get; set; }
        public string Comentario { get; set; }
        public string CadenaFotoFrente { get; set; }
        public string CadenaFotoDerecha { get; set; }
        public string CadenaFotoIzquierda { get; set; }
        public string RutaFotoAtras { get; set; }
    }
}
