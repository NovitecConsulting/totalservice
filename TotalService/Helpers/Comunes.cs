﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TotalService.Helpers
{
    public class Comunes
    {
        public const string ROL_Operador = "Operador";
        public const string ROL_Administrador = "Administrador";
        public const string ROL_Supervisor = "Supervisor";
    }
}