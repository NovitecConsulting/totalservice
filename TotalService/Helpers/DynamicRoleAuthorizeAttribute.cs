﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TotalService.Helpers
{

    public class RoleProvider
    {
        public string[] Get(string controller, string action)
        {
            // get your roles based on the controller and the action name 
            // wherever you want such as db
            // I hardcoded for the sake of simplicity 
            return new string[] { "Student", "Teacher" };
        }
    }

    public class DynamicRoleAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var controller = httpContext.Request.RequestContext
                .RouteData.GetRequiredString("controller");
            var action = httpContext.Request.RequestContext
                .RouteData.GetRequiredString("action");

            RoleProvider _rolesProvider = new RoleProvider();

            // feed the roles here
            Roles = string.Join(",", _rolesProvider.Get(controller, action));

            return base.AuthorizeCore(httpContext);
        }
    }
}